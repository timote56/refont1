<?php
    $path='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $current=basename($path);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>kercode-basketball</title>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="mediaqueries.css">
        <link rel="stylesheet" href="keyframes.css">
        
    </head>
    <body>
        <header id="banner">
            <div class="flexmenu">
                <figure class="logo">
                    <a href="index.php">
                        <img src="images/logobasket.png" alt="logo basketball">
                    </a>
                </figure>
                <nav id="menu">
                    <div class="burger">
                        <i class="fas fa-bars"></i>
                    </div>
                    <ul>
                        <li><a href="index.php">accueil</a></li>
                        <li><a href="cours.php">cours</a></li>
                        <li><a href="contact.php">contact</a></li>
                    </ul>
                </nav>
            </div>
            
            
            
        </header>