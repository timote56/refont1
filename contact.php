<?php
    include("header.php");
?>
<main id="content">
        <div class="pagewidth">
            <div id="contact">
                <h1>Contactez nous</h1>
                <form action="envoimail.php" method="post">
                    <fieldset>
                        <legend> Vos Coordonnées&nbsp;</legend>
                        <div class="formcolumn">
                            <p>
                                <label for="nom">Nom </label>
                                <input type="text" name="nom" id="nom" placeholder="tapez votre nom">
                            </p>
                            <p>
                                <label for="prenom">Prénom </label>
                                <input type="text" name="prenom" id="prenom" placeholder="tapez votre prénom">
                            </p>
                            <p>
                                <label for="email">Email </label>
                                <input type="email" name="email" id="email" placeholder="toto@toto.com">
                            </p>
                            <p>
                                <label for="tel">Téléphone </label>
                                <input type="tel" name="tel" id="tel" placeholder="06 06 06 06 06">
                            </p>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend> Votre Message&nbsp;</legend>
                        <p>
                            <label for="message">Votre message </label>
                            <textarea name="message" id="message" placeholder="votre message"></textarea>
                        </p>
                    </fieldset> 
                    <p class="button-form"><input type="submit" value="ENVOYER">
                    <input type="reset" value="ANNULER"></p>
                </form>
            </div>
            <div id="map">
                <h2>Nous trouver </h2>
                <iframe id="carte" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7601.126622911442!2d-2.7553610419263896!3d47.65714583155205!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa95679d70af65df9!2sClub%20Gym!5e0!3m2!1sfr!2sfr!4v1604672186696!5m2!1sfr!2sfr" 
                frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
          
                <address>
                    <strong>Club de Gym</strong> <br>
                    17 rue de la Tannerie <br>
                    56000 VANNES
                </address>
            </div>
        </div>
    </main>
<?php
    include("footer.php");
?>